/*
 * adc.c
 *
 *  Created on: 12 Mar 2018
 *      Author: 19837739
 *      extended by 19857225
 */

#include "adc.h"
#include "SevSeg.h"
#include "uart.h"
#include <math.h>

uint16_t adc1_Val[4];

uint16_t CH1_midVal = 0;
uint16_t CH1_oldVal = 0;

uint16_t CH2_midVal = 0;
uint16_t CH2_oldVal = 0;

int fastCurrentRMS = 0;
int fastVoltageRMS = 0;
int fastTemp = 0;
int fastHotWater = 0;
int currentRMS = 0;
int voltageRMS = 0;
int currentHeatElementState;
int valveValue;
uint32_t ambientTemp = 0;
uint32_t waterTemp = 0;

uint8_t sumCount = 0;

float rmsCurrentSum = 0;
float rmsVoltageSum = 0;
float sampleCurrent = 0;
float sampleVoltage = 0;
float sampleAmbientTemp = 0;
float sampleWater = 0;

int heatingSetting;
void adcInit()
{

}

void adcLoop()
{
	HAL_ADC_Start_DMA(&hadc1, (uint32_t*)adc1_Val, 4);
	fastFindMax(adc1_Val);
	findRMS(adc1_Val);
	findAmbientTemp(adc1_Val);
	findWaterTemp(adc1_Val);
}

// Finds the peak of the wave and calculates rms from that. Will only work with pure sine waves.
void fastFindMax(uint16_t* newVal)
{
	if(CH1_midVal > newVal[0] && CH1_midVal > CH1_oldVal)
	{
		fastCurrentRMS = map((float)CH1_midVal, adc_curr_min, adc_curr_max, -13000, 13000)*0.70711;
	}
	CH1_oldVal = CH1_midVal;
	CH1_midVal = newVal[0];

	if(CH2_midVal > newVal[1] && CH2_midVal > CH2_oldVal)
	{
		fastVoltageRMS = map((float)CH2_midVal, adc_volt_min, adc_volt_max, -220000, 220000)*0.70711;
	}
	CH2_oldVal = CH2_midVal;
	CH2_midVal = newVal[1];
}

// Takes N samples and calculates rms from that
void findRMS(uint16_t* newVal)
{
	sampleCurrent = map((float)newVal[0], adc_curr_min, adc_curr_max, -13000, 13000); // normalise

	rmsCurrentSum = rmsCurrentSum + (sampleCurrent*sampleCurrent);

	sampleVoltage = map((float)newVal[1], adc_volt_min, adc_volt_max, -220000, 220000); // normalise

	rmsVoltageSum = rmsVoltageSum + (sampleVoltage*sampleVoltage);

	if(++sumCount == N)
	{
		voltageRMS = sqrt(rmsVoltageSum/N);
		currentRMS = sqrt(rmsCurrentSum/N);

		rmsVoltageSum = 0;
		rmsCurrentSum = 0;
		sumCount = 0;
	}
}

//Calculate ambient temperature from ADC sampled values
void findAmbientTemp(uint16_t* newVal){

	sampleAmbientTemp = (((float)newVal[2]/4096) * 3300);

	ambientTemp = ((float)(sampleAmbientTemp - 500)/10);
}

//Calculate value for hot water from ADC sampled values
void findWaterTemp(uint16_t* newVal){

	sampleWater = (((float)newVal[3]/4096) * 3300);

	waterTemp = ((float)(sampleWater - 500)/10);
}

//Set heat element on or off
void setHeatElement(uint16_t newVal){
	 currentHeatElementState = newVal;
	if(newVal == 0) {
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_14, 0);
	} else
	{
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_14, 1);
	}
}

//Set valve open or closed
void setValve(uint16_t newVal){
	 valveValue = newVal;
	if(newVal == 0) {
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_15, 0);

	} else {
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_15, 1);
	}
}

// taken from arduino source code
// used to change a number in a certain range to a new range
float map(float x, float in_min, float in_max, float out_min, float out_max)
{
	return (x - in_min)*(out_max - out_min)/(in_max - in_min) + out_min;
}
