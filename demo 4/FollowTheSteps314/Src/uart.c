/*
 * uart.c
 *
 *  Created on: 08 Mar 2018
 *  Author: 19837739 and lvisagie (28 Feb 2018) and 19857225
 *
 */

#include "uart.h"
#include <stdbool.h>
#include "SevSeg.h"
#include "stm32f3xx_it.h"

char rxchar;         // temporary storage - if a character arrives from the UART, it will be placed in this variable
#define CMDBUFLEN 60   // maximum length of a command string received on the UART
#define MAXTXLEN  60   // maximum length of transmit buffer (replies sent back to UART)
char cmdbuffer[CMDBUFLEN];  // buffer in which to store commands that are received from the UART
uint16_t cmdbufpos;  // this is the position in the cmdbuffer where we are currently writing to

char* TXMSG_STUDENTNUM = "$A,19857225\r\n";  // this is the string that will be returned if a $A command is received
char txbuffer[MAXTXLEN];     // buffer for replies that are to be sent out on UART

int waterFlowSetting;
int counter;
int halStatus;
int i ;
int currentHeatElementState;
int val;
int start_time_in_seconds;
int end_time_in_seconds;
int current_time;
int start_time_in_seconds1;
int end_time_in_seconds1;
int current_time1;
int start_time_in_seconds2;
int end_time_in_seconds2;
int current_time2;
int start_time_in_seconds3;
int end_time_in_seconds3;
int current_time3;
int window_1=0;
int window_2=0;
int window_3=0;
bool AutoHeating = 0;

char arr_rtc[11];
int16_t hrs = 0;
int16_t min = 0;
int16_t sec = 0;
uint16_t hrs_start = 0;
uint16_t min_start = 0;
uint16_t sec_start = 0;
uint16_t hrs_end = 0;
uint16_t min_end = 0;
uint16_t sec_end = 0;
uint16_t hrs_start1 = 0;
uint16_t min_start1= 0;
uint16_t sec_start1 = 0;
uint16_t hrs_end1 = 0;
uint16_t min_end1 = 0;
uint16_t sec_end1 = 0;
uint16_t hrs_start2 = 0;
uint16_t min_start2 = 0;
uint16_t sec_start2 = 0;
uint16_t hrs_end2 = 0;
uint16_t min_end2 = 0;
uint16_t sec_end2 = 0;
uint16_t hrs_start3 = 0;
uint16_t min_start3 = 0;
uint16_t sec_start3 = 0;
uint16_t hrs_end3 = 0;
uint16_t min_end3 = 0;
uint16_t sec_end3 = 0;
uint16_t heatWindowNum;
RTC_TimeTypeDef heatScheduling [3][2] = {0}; // [3] window number en [2] on or off state

void uartInit(void)
{
	// tell the UART we want to be interrupted after one character was received
	HAL_UART_Receive_IT(&huart1, (uint8_t*)&rxchar, 1);
}

// HAL_UART_RxCpltCallback - callback function that will be called from the UART interrupt handler. This function will execute whenever a character is received from the UART
void HAL_UART_RxCpltCallback(UART_HandleTypeDef* huart)
{
	// the interrupt handler will automatically put the received character in the rxchar variable (no need to write any code for that).
	// if it is a $ character, clear the command buffer first
	if (rxchar == '$')
		cmdbufpos = 0;

	// add character to command buffer, but only if there is more space in the command buffer
	if (cmdbufpos < CMDBUFLEN)
		cmdbuffer[cmdbufpos++] = rxchar;

	// check if there is a complete command in the cmdbuffer
	if (cmdbuffer[cmdbufpos-1] == '\n')
	{
		// call a helper function to decode the command
		DecodeCommand();

		// clear buffer
		cmdbufpos = 0;
	}

	// tell the UART we want to be interrupted after another character was received
	HAL_UART_Receive_IT(&huart1, (uint8_t*)&rxchar, 1);
}


// DecodeCommand - decode and act on commands received from the UART
void DecodeCommand()
{
	uint8_t numcharswritten;
	uint8_t totalCharsWritten = 0;


	if(cmdbuffer[0] != '$')
	{
		HAL_UART_Transmit(&huart1, (uint8_t*)"Invalid command: Missing $\r\n", 28, 1000);
	}
	else
	{
		switch (cmdbuffer[1])
		{
		// student number request
		case 'A' :
			// return string %A,<student num><CR><LF>  (length will always be 13 bytes)
			HAL_UART_Transmit(&huart1, (uint8_t*)TXMSG_STUDENTNUM, 13, 1000);
			break;

		case 'B' :
			//return Valve setting(OPEN/CLOSED)
			String2Int(cmdbuffer+3, &ValveSwitch);


			txbuffer[0] = '$'; txbuffer[1] = 'B'; txbuffer[2] = '\r'; txbuffer[3] = '\n';

			if(cmdbuffer[3] == 1){
				valveValue = 1;
			}
			if(cmdbuffer[3] == 0){
				valveValue = 0;
			}

			setValve(ValveSwitch);
			HAL_UART_Transmit(&huart1, (uint8_t*)txbuffer, 4, 1000);
			break;

		case 'C' :
			//
			if(cmdbuffer[3] == '1')
			{
				AutoHeating = 1;
			}

			if(cmdbuffer[3] == '0')
			{
				AutoHeating = 0;
			}
			txbuffer[0] = '$'; txbuffer[1] = 'C'; txbuffer[2] = '\r'; txbuffer[3] = '\n';
			HAL_UART_Transmit(&huart1, (uint8_t*)txbuffer, 4, 1000);
			break;
			break;

		case 'D' :
			//return heater setting(ON/OFF)
			//parse value of setting
			String2Int(cmdbuffer+3, &heatingSwitch);
			setHeatElement(heatingSwitch);


			if(cmdbuffer[3] ==1) {
				val=1;
			}
			if(cmdbuffer[3] ==0){
				val=0;
			}

			txbuffer[0] = '$'; txbuffer[1] = 'D'; txbuffer[2] = '\r'; txbuffer[3] = '\n';
			HAL_UART_Transmit(&huart1, (uint8_t*)txbuffer, 4, 1000);
			break;

		case 'E' :
			//
			break;

			// set temperature set-point
		case 'F':
			// parse temperature value
			String2Int(cmdbuffer+3, &temp_setpoint);

			// return string %F<CR><LF>  (length will always be 4 bytes)
			txbuffer[0] = '$'; txbuffer[1] = 'F'; txbuffer[2] = '\r'; txbuffer[3] = '\n';
			HAL_UART_Transmit(&huart1, (uint8_t*)txbuffer, 4, 1000);
			break;

			// get temperature set-point
		case 'G':
			// return string with the format $G,<temp><CR><LF>
			txbuffer[0] = '$'; txbuffer[1] = 'G'; txbuffer[2] = ',';
			numcharswritten = Int2String(txbuffer+3, temp_setpoint, 4);
			txbuffer[3+numcharswritten] = '\r'; txbuffer[4+numcharswritten] = '\n';
			HAL_UART_Transmit(&huart1, (uint8_t*)txbuffer, numcharswritten+5, 1000);
			break;

		case 'H' :
			i = 3;
			numcharswritten = String2Int(&cmdbuffer[i],&hrs);

			if(hrs>9)
			{
				i = i + 3;
			}
			else i = i + 2;

			numcharswritten = String2Int(&cmdbuffer[i], &min);
			if (min>9)
			{
				i = i + 3;
			}
			else i = i + 2;

			String2Int(&cmdbuffer[i],&sec);
			set_time();
			txbuffer[0] = '$'; txbuffer[1] = 'H'; txbuffer[2] = '\r'; txbuffer[3] = '\n';
			HAL_UART_Transmit(&huart1, (uint8_t*)txbuffer, 4, 1000);
			break;
			break;

		case 'I' :
			//
			get_time();
			txbuffer[0] = '$'; txbuffer[1] = 'I'; txbuffer[2] = ',';
			totalCharsWritten = 3;

			numcharswritten = Int2String(txbuffer+totalCharsWritten, time1.Hours, 2);
			totalCharsWritten += numcharswritten;

			txbuffer[totalCharsWritten++] = ',';
			numcharswritten = Int2String(txbuffer+totalCharsWritten, time1.Minutes, 2);
			totalCharsWritten += numcharswritten;

			txbuffer[totalCharsWritten++] = ',';
			numcharswritten = Int2String(txbuffer+totalCharsWritten, time1.Seconds, 2);
			totalCharsWritten += numcharswritten;

			txbuffer[totalCharsWritten++] = '\r'; txbuffer[totalCharsWritten++] = '\n';
			HAL_UART_Transmit(&huart1, (uint8_t*)txbuffer, totalCharsWritten, 1000);

			break;
			break;

		case 'J' :
			//
			txbuffer[0] = '$'; txbuffer[1] = 'J'; txbuffer[2] = '\r'; txbuffer[3] = '\n';
			HAL_UART_Transmit(&huart1, (uint8_t*)txbuffer, 4, 1000);

			i = 5;
			numcharswritten = String2Int(&cmdbuffer[i], &hrs_start);

			if(hrs_start>9)
			{
				i = i + 3;
			}
			else i = i + 2;

			numcharswritten = String2Int(&cmdbuffer[i], &min_start);
			if (min_start>9)
			{
				i = i + 3;
			}
			else i = i + 2;

			String2Int(&cmdbuffer[i],&sec_start);
			if (sec_start>9)
			{
				i = i + 3;
			}
			else i = i + 2;


			numcharswritten = String2Int(&cmdbuffer[i], &hrs_end);

			if(hrs_end>9)
			{
				i = i + 3;
			}
			else i = i + 2;

			numcharswritten = String2Int(&cmdbuffer[i], &min_end);
			if (min_end>9)
			{
				i = i + 3;
			}
			else i = i + 2;

			String2Int(&cmdbuffer[i], &sec_end);
			if (sec_end>9)
			{
				i = i + 3;
			}
			else i = i + 2;


			if(cmdbuffer[3]=='1')
			{	//window 1
				//case 49 :

				hrs_start1 = hrs_start;
				min_start1 = min_start;
				sec_start1 = sec_start;
				hrs_end1 = hrs_end;
				min_end1 = min_end;
				sec_end1 = sec_end;

				start_time_in_seconds1 = ((hrs_start1*60 + min_start1)*60) + sec_start1;

				end_time_in_seconds1 = ((hrs_end1*60 + min_end1)*60) + sec_end1;

				current_time1 =  ((time1.Hours*60 + time1.Minutes)*60) + time1.Seconds;
				//break;
			}
			//handleHeatingElement(int start_time_in_seconds1; int end_time_in_seconds1; int current_time1);

			//window 2
			//case 50 :
			if(cmdbuffer[3]=='2'){
				heatScheduling[1][0] = time2;
				heatScheduling[1][1] = time3;
				hrs_start2 = hrs_start;
				min_start2 = min_start;
				sec_start2 = sec_start;
				hrs_end2 = hrs_end;
				min_end2 = min_end;
				sec_end2 = sec_end;



				//convert start and end times from 24hr format to time in seconds
				start_time_in_seconds2 = ((hrs_start2*60 + min_start2)*60) + sec_start2;

				end_time_in_seconds2 = ((hrs_end2*60 + min_end2)*60) + sec_end2;

				current_time1 =  ((time1.Hours*60 + time1.Minutes)*60) + time1.Seconds;
				//	break;
			}

			//window 3
			//case 51 :
			if(cmdbuffer[3]=='3')
			{
				heatScheduling[2][0] = time2;
				heatScheduling[2][1] = time3;

				//convert start and end times from 24hr format to time in seconds
				hrs_start3 = hrs_start;
				min_start3 = min_start;
				sec_start3 = sec_start;
				hrs_end3 = hrs_end;
				min_end3 = min_end;
				sec_end3 = sec_end;

				start_time_in_seconds3 = ((hrs_start3*60 + min_start3)*60) + sec_start3;

				end_time_in_seconds3 = ((hrs_end3*60 + min_end3)*60) + sec_end3;

				current_time1 =  ((time1.Hours*60 + time1.Minutes)*60) + time1.Seconds;
			}

			break;

		case 'K' :
			// preamble
			txbuffer[0] = '$'; txbuffer[1] = 'K'; txbuffer[2] = ',';
			totalCharsWritten = 3;

			// arg 1: current RMS
			numcharswritten = Int2String(&txbuffer[totalCharsWritten], currentRMS, 5);
			totalCharsWritten += numcharswritten;
			txbuffer[totalCharsWritten++] = ',';

			// arg 2: voltage RMS
			numcharswritten = Int2String(&txbuffer[totalCharsWritten], voltageRMS, 6);
			totalCharsWritten += numcharswritten;
			txbuffer[totalCharsWritten++] = ',';

			// arg 3: ambient temp
			numcharswritten = Int2String(&txbuffer[totalCharsWritten], ambientTemp, 3);
			totalCharsWritten += numcharswritten;
			txbuffer[totalCharsWritten++] = ',';

			// arg 4: water temp
			numcharswritten = Int2String(&txbuffer[totalCharsWritten], waterTemp, 3);

			totalCharsWritten += numcharswritten;
			//txbuffer[totalCharsWritten++] = '0';
			txbuffer[totalCharsWritten++] = ',';

			// arg 5: accumulated water flow
			numcharswritten = Int2String(&txbuffer[totalCharsWritten], getCount(), 10);
			totalCharsWritten += numcharswritten;
			//txbuffer[totalCharsWritten++] = waterFlowSetting;
			txbuffer[totalCharsWritten++] = ',';

			// arg 6: element status

			if(currentHeatElementState == 0){

				txbuffer[totalCharsWritten++] = 'O'; txbuffer[totalCharsWritten++] = 'F'; txbuffer[totalCharsWritten++] = 'F';
				txbuffer[totalCharsWritten++] = ',';
			}

			else if(currentHeatElementState == 1)
			{
				txbuffer[totalCharsWritten++] = 'O'; txbuffer[totalCharsWritten++] = 'N';
				//txbuffer[totalCharsWritten++] = '\r'; txbuffer[totalCharsWritten++] = '\n';
				txbuffer[totalCharsWritten++] = ',';
			}

			// arg 7: valve status

			if(valveValue == 0)
			{
				txbuffer[totalCharsWritten++] = 'C'; txbuffer[totalCharsWritten++] = 'L'; txbuffer[totalCharsWritten++] = 'O';
				txbuffer[totalCharsWritten++] = 'S'; txbuffer[totalCharsWritten++] = 'E'; txbuffer[totalCharsWritten++] = 'D';
			}

			if(valveValue == 1)
			{
				txbuffer[totalCharsWritten++] = 'O'; txbuffer[totalCharsWritten++] = 'P'; txbuffer[totalCharsWritten++] = 'E'; txbuffer[totalCharsWritten++] = 'N';
			}

			// \r\n
			txbuffer[totalCharsWritten++] = '\r'; txbuffer[totalCharsWritten++] = '\n';

			HAL_UART_Transmit(&huart1, (uint8_t*)txbuffer, totalCharsWritten, 1000);
			break;
		}
	}
}

bool getAutoHeating(){
	return AutoHeating;
}

void handleHeatingElement()
{
	//EXECUTE FOR WINDOW 1

	//getTime()
	current_time =  ((time1.Hours*60 + time1.Minutes)*60) + time1.Seconds;

	if (end_time_in_seconds1 > start_time_in_seconds1)
	{
		if (current_time >= start_time_in_seconds1 && current_time <= end_time_in_seconds1 && window_2==0 && window_3==0)
		{
			window_1=1;
			if(waterTemp < (temp_setpoint+5))
			{
				currentHeatElementState=1;
			}

			if(waterTemp > (temp_setpoint+5))
			{
				currentHeatElementState=0;
			}
			setHeatElement(currentHeatElementState);
		}
		else if(window_1==1){
			currentHeatElementState = 0;
			window_1=0;
			setHeatElement(currentHeatElementState);
		}
	}

	if (end_time_in_seconds2 > start_time_in_seconds2)
	{
		if (current_time >= start_time_in_seconds2 && current_time <= end_time_in_seconds2 && window_1==0 && window_3==0)
		{
			window_2 = 1;
			if(waterTemp < (temp_setpoint+5))
			{
				currentHeatElementState=1;
			}

			if(waterTemp >(temp_setpoint+5))
			{
				currentHeatElementState=0;
			}
			setHeatElement(currentHeatElementState);
		}
		else if(window_2 == 1){
			window_2=0;
			currentHeatElementState = 0;
			setHeatElement(currentHeatElementState);
		}
	}
	if (end_time_in_seconds3 > start_time_in_seconds3)
	{
		if (current_time >= start_time_in_seconds3 && current_time <= end_time_in_seconds3 && window_2==0 && window_1==0)
		{
			window_3=1;
			if(waterTemp < (temp_setpoint+5))
			{
				currentHeatElementState=1;
			}

			if(waterTemp >(temp_setpoint+5))
			{
				currentHeatElementState=0;
			}
			setHeatElement(currentHeatElementState);
		}
		else if (window_3 ==1){
			window_3=0;
			currentHeatElementState = 0;
			setHeatElement(currentHeatElementState);
		}

	}

}

void set_time(){

	time1.Hours = hrs;
	time1.Minutes = min;
	time1.Seconds = sec;
	date1.WeekDay = RTC_WEEKDAY_MONDAY;
	date1.Month = RTC_MONTH_JANUARY;
	date1.Year = 0;
	date1.Date = 1;

	__HAL_RTC_WRITEPROTECTION_DISABLE(&hrtc); // Disable write protection
	halStatus = RTC_EnterInitMode(&hrtc); // Enter init mode
	halStatus = HAL_RTC_SetTime(&hrtc, &time1, RTC_FORMAT_BIN);
	halStatus = HAL_RTC_SetDate(&hrtc, &date1, RTC_FORMAT_BIN);
	__HAL_RTC_WRITEPROTECTION_ENABLE(&hrtc);

}

void get_time()
{
	halStatus = HAL_RTC_GetTime(&hrtc, &time1, RTC_FORMAT_BIN);
	halStatus = HAL_RTC_GetDate(&hrtc, &date1, RTC_FORMAT_BIN);
}

// String2Int - convert ASCII string to integer variable
uint8_t String2Int(char* input_string, int16_t* output_integer)
{
	int retval = 0;

	if (*input_string == '\0')
		return 0;

	int sign = 1;
	if (*input_string == '-')
	{
		sign = -1;
		input_string++;
	}

	while ((*input_string >= '0') && (*input_string <= '9'))
	{
		retval *= 10;
		retval += (*input_string - 48);

		if (((sign == 1) && (retval >= 32768)) ||
				((sign == -1) && (retval >= 32769)))
			return 0;

		input_string++;
	}
	*output_integer = (int16_t)(sign * retval);
	return 1;
}

// convert integer var to ASCII string
uint8_t Int2String(char* output_string, int32_t val, uint8_t maxlen)
{
	if (maxlen == 0)
		return 0;

	int numwritten = 0;

	if (val < 0)
	{
		output_string[0] = '-';
		output_string++;
		maxlen--;
		val = -val;
		numwritten = 1;
	}

	uint8_t digits = 0;
	if (val < 10)
		digits = 1;
	else if (val < 100)
		digits = 2;
	else if (val < 1000)
		digits = 3;
	else if (val < 10000)
		digits = 4;
	else if (val < 100000)
		digits = 5;
	else
		digits = 6;

	if (digits > maxlen)
		return 0; // error - not enough space in output string!

	int writepos = digits;
	while (writepos > 0)
	{
		output_string[writepos-1] = (char) ((val % 10) + 48);
		val /= 10;
		writepos--;
		numwritten++;
	}

	return numwritten;
}
