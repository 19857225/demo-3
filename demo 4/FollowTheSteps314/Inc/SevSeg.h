/*
 * 7Seg.h
 *
 *  Created on: 08 Mar 2018
 *      Author: 19837739
 */

#ifndef SEVSEG_H_
#define SEVSEG_H_

#include "main.h"
#include "stm32f3xx_hal.h"

// extern variable declarations - tell the compiler that these variables exist, and are defined elsewhere
extern int16_t temp_setpoint; // the current temperature setpoint
extern int it_count;

// function prototypes
void SevSegInit (void);

void SevSegLoop(int16_t num);

void SevSegwrite(uint8_t num);

void SevSegReset();



#endif /* SEVSEG_H_ */
