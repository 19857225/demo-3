/*
 * uart.h
 *
 *  Created on: 08 Mar 2018
 *      Author: 19837739
 */

#ifndef UART_H_
#define UART_H_

#include <stdbool.h>
#include "main.h"
#include "stm32f3xx_hal.h"

// extern variable declarations - tell the compiler that these variables exist, and are defined elsewhere
extern UART_HandleTypeDef huart1;
extern int16_t temp_setpoint; // the current temperature setpoint
extern int16_t heatingSwitch;
extern int16_t ValveSwitch;
extern int heatingSetting;
extern int fastCurrentRMS;
extern int fastVoltageRMS;
extern int currentRMS;
extern int voltageRMS;
extern uint32_t ambientTemp;
extern uint32_t waterTemp;
extern int val;
extern int valveValue;
extern RTC_TimeTypeDef sTime;
RTC_TimeTypeDef time1;
RTC_TimeTypeDef time2;
RTC_TimeTypeDef time3;
RTC_DateTypeDef date1;

extern RTC_HandleTypeDef hrtc;
extern int16_t hrs;
extern int16_t min;
extern int16_t sec;
// function prototypes

void DecodeCommand(void);

void uartInit(void);

// UserMainLoopUpdate - function that gets called from main, with our user code in it. This function does not in itself contain a loop, but is called with every iteration of the main loop.
void uartLoopCheck(void);

void helloWorld();

uint8_t Int2String(char* output_string, int32_t val, uint8_t maxlen);

uint8_t String2Int(char* input_string, int16_t* output_integer);

void setHeatElement(uint16_t newVal);

void setValve(uint16_t newVal);

void rtc_unit(char *arr_rtc);

void set_time();

void get_time();

bool getAutoHeating();

void handleHeatingElement();

#endif /* UART_H_ */
